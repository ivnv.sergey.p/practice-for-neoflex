package ru.neoflex.practice.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "calculation_results")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CalculationResult {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String operation;
    private Integer operand1;
    private Integer operand2;
    private Integer result;
}
