package ru.neoflex.practice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.neoflex.practice.model.CalculationResult;
import ru.neoflex.practice.repository.CalculationResultRepository;

import java.util.List;

@RestController
@RequestMapping("/api/v1/calculations")
public class CalculationResultController {

    @Autowired
    private CalculationResultRepository calculationResultRepository;

    @GetMapping
    public List<CalculationResult> getAllCalculations() {
        return calculationResultRepository.findAll();
    }

    @PostMapping
    public CalculationResult createCalculation(@RequestBody CalculationResult calculationResult) {
        return calculationResultRepository.save(calculationResult);
    }
}
