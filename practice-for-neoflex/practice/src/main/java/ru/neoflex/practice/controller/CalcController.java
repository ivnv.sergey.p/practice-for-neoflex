package ru.neoflex.practice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.neoflex.practice.model.CalculationResult;
import ru.neoflex.practice.repository.CalculationResultRepository;

@RestController
@RequestMapping("/calc")
@Tag(name = "Calculator API", description = "API для выполнения математических операций")
public class CalcController {

    @Autowired
    private CalculationResultRepository calculationResultRepository;

    @GetMapping("/{operation}/{operand1}/{operand2}")
    @Operation(summary = "Выполнить математическую операцию", description = "Выполняет указанную операцию над двумя операндами и сохраняет результат в базу данных")
    public CalculationResult calculate(@PathVariable String operation, @PathVariable int operand1, @PathVariable int operand2) {
        int result = 0;
        switch (operation) {
            case "add":
                result = operand1 + operand2;
                break;
            case "subtract":
                result = operand1 - operand2;
                break;
            case "multiply":
                result = operand1 * operand2;
                break;
            case "divide":
                if (operand2 == 0) {
                    throw new IllegalArgumentException("Деление на ноль невозможно");
                }
                result = operand1 / operand2;
                break;
            default:
                throw new IllegalArgumentException("Invalid operation: " + operation);
        }

        CalculationResult calculationResult = new CalculationResult(null, operation, operand1, operand2, result);
        return calculationResultRepository.save(calculationResult);
    }
}
