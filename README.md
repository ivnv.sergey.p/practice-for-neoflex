# Practice for Neoflex

## Описание

Это простое Spring Boot приложение для выполнения базовых арифметических операций (сложение, вычитание и умножение) с использованием REST API и Swagger UI для документации API.

## Установка

1. **Клонируйте репозиторий:**

```sh
git clone git@gitlab.com:ivnv.sergey.p/practice-for-neoflex.git
cd practice
```

2. **Соберите проект:**
```sh
mvn clean install
```
## Запуск приложения
1. **Запустите Spring Boot приложение:**
```sh
mvn spring-boot:run
```
Или вы можете запустить приложение из вашей IDE, запустив класс PracticeApplication.

## Тестирование приложения
После запуска приложения, вы можете протестировать API, используя следующие эндпоинты:

Сложение: http://localhost:8080/plus/{a}/{b}

Вычитание: http://localhost:8080/minus/{a}/{b}

Умножение: http://localhost:8080/multiply/{a}/{b}

Замените {a} и {b} на целые числа, которые вы хотите сложить, вычесть или умножить.

Swagger UI
Swagger UI доступен для тестирования и документации API:

Откройте ваш браузер и перейдите по адресу http://localhost:8080/swagger-ui.html

## Swagger UI

![Alt text](<Screenshot/Снимок экрана 2024-05-31 153737.png>)
![Alt text](<Screenshot/Снимок экрана 2024-05-31 153752.png>)
![Alt text](<Screenshot/Снимок экрана 2024-05-31 153835.png>)
![Alt text](<Screenshot/Снимок экрана 2024-05-31 153851.png>)
## Автор


# Практика от 16.09
## Скриншоты выполнения
![Alt text](<Screenshot/newpract/photo_1_2024-09-20_11-03-26.jpg>)
![Alt text](<Screenshot/newpract/photo_2_2024-09-20_11-03-26.jpg>)
![Alt text](<Screenshot/newpract/photo_3_2024-09-20_11-03-26.jpg>)
![Alt text](<Screenshot/newpract/photo_4_2024-09-20_11-03-26.jpg>)
![Alt text](<Screenshot/newpract/photo_5_2024-09-20_11-03-26.jpg>)
## 2 неделя
![Alt text](<Screenshot/newpract/2week/New (1).jpg>)
![Alt text](<Screenshot/newpract/2week/New (2).jpg>)
![Alt text](<Screenshot/newpract/2week/New (3).jpg>)
![Alt text](<Screenshot/newpract/2week/New (4).jpg>)
![Alt text](<Screenshot/newpract/2week/New (5).jpg>)
![Alt text](<Screenshot/newpract/2week/New (6).jpg>)
![Alt text](<Screenshot/newpract/2week/New (7).jpg>)
![Alt text](<Screenshot/newpract/2week/New (8).jpg>)
![Alt text](<Screenshot/newpract/2week/New (9).jpg>)
![Alt text](<Screenshot/newpract/2week/New (10).jpg>)
![Alt text](<Screenshot/newpract/2week/New (11).jpg>)
![Alt text](<Screenshot/newpract/2week/New (12).jpg>)
![Alt text](<Screenshot/newpract/2week/New (13).jpg>)
![Alt text](<Screenshot/newpract/2week/New (14).jpg>)
![Alt text](<Screenshot/newpract/2week/New (15).jpg>)
![Alt text](<Screenshot/newpract/2week/New (16).jpg>)
## 3 неделя
Седьмое
![Alt text](<Screenshot/7-14/Седьмая (1).jpg>)
![Alt text](<Screenshot/7-14/Седьмая (2).jpg>)
![Alt text](<Screenshot/7-14/Седьмая (3).jpg>)
![Alt text](<Screenshot/7-14/Седьмая (4).jpg>)
Восьмое
![Alt text](<Screenshot/7-14/Восьмая.jpg>)
Девятое
![Alt text](<Screenshot/7-14/Девятая (1).jpg>)
![Alt text](<Screenshot/7-14/Девятая (2).jpg>)
![Alt text](<Screenshot/7-14/Девятая (3).jpg>)
![Alt text](<Screenshot/7-14/Девятая (4).jpg>)
Десятое
![Alt text](<Screenshot/7-14/Десятое (1).jpg>)
![Alt text](<Screenshot/7-14/Десятое (2).jpg>)
![Alt text](<Screenshot/7-14/Десятое (3).jpg>)
Одинадцатое
![Alt text](<Screenshot/7-14/Одинадцатое (1).jpg>)
![Alt text](<Screenshot/7-14/Одинадцатое (2).jpg>)
![Alt text](<Screenshot/7-14/Одинадцатое (3).jpg>)
![Alt text](<Screenshot/7-14/Одинадцатое (4).jpg>)
Двенадцатое
![Alt text](<Screenshot/7-14/Двенадцатое (1).jpg>)
![Alt text](<Screenshot/7-14/Двенадцатое (2).jpg>)
![Alt text](<Screenshot/7-14/Двенадцатое (3).jpg>)
Тринадцатое
![Alt text](<Screenshot/7-14/Тринадцатое.jpg>)
Четырнадцатое
![Alt text](<Screenshot/7-14/Четырнадцатое.jpg>)
Иванов Сергей

