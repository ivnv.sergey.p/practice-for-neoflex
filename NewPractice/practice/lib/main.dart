import 'package:flutter/material.dart';
import 'lecture_checker.dart'; // Файл для проверки наличия программ
import 'lecture_pages.dart';  // Файл для отображения страниц с названиями лекций

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Лекции',
      theme: ThemeData(primarySwatch: Colors.blue),
      home: LectureListPage(),
    );
  }
}

class LectureListPage extends StatelessWidget {
  final List<String> lectureNames = [
    'Лекция 2',
    'Лекция 3',
    'Лекция 4',
    'Лекция 5',
    'Лекция 6',
    'Лекция 7',
    'Лекция 8',
    'Лекция 9',
    'Лекция 10',
    'Лекция 11',
    'Лекция 12',
    'Лекция 13',
    'Лекция 14',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Список Лекций'),
      ),
      body: ListView.builder(
        itemCount: lectureNames.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(lectureNames[index]),
            onTap: () async {
              bool hasProgram = await hasLectureProgram(index + 2);  // Проверка наличия программы для каждой лекции
              if (hasProgram) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => runLectureProgram(index + 2),  // Запуск программы, если она существует
                  ),
                );
              } else {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => LecturePage(lectureNames[index]),  // Показ страницы с названием лекции, если программы нет
                  ),
                );
              }
            },
          );
        },
      ),
    );
  }
}
