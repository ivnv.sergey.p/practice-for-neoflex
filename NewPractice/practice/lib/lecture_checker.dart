import 'package:flutter/material.dart';
import 'lecture_pages.dart';
import 'pages/lecture_page_2.dart';  // Импорт файлов программ из папки pages
import 'pages/lecture_page_3.dart';
import 'pages/lecture_page_4.dart';
import 'pages/lecture_page_5.dart';
import 'pages/lecture_page_6.dart';
import 'pages/lecture_page_7.dart';
import 'pages/lecture_page_8.dart';
import 'pages/lecture_page_9.dart';
import 'pages/lecture_page_10.dart';
import 'pages/lecture_page_11.dart';
import 'pages/lecture_page_12.dart';
import 'pages/lecture_page_13.dart';
import 'pages/lecture_page_14.dart';
// Добавь остальные программы

Future<bool> hasLectureProgram(int lectureNumber) async {
  // Проверка наличия программы для каждой лекции
  switch (lectureNumber) {
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
      return true;  // Программы для Лекции 2 и Лекции 3
  // Добавь другие лекции
    default:
      return false;  // Если программы нет
  }
}

Widget runLectureProgram(int lectureNumber) {
  switch (lectureNumber) {
    case 2:
      return LecturePage2();  // Запуск программы Лекции 2
    case 3:
      return LecturePage3();
    case 4:
      return LecturePage4();
    case 5:
     return LecturePage5();
    case 6:
     return LecturePage6();
    case 7:
   //   return LecturePage7();
    case 8:
    //  return LecturePage8();
    case 9:
   //   return LecturePage9();
    case 10:
    //  return LecturePage10();
    case 11:
    //  return LecturePage11();
    case 12:
    //  return LecturePage12();
    case 13:
    //  return LecturePage13();
    case 14:
    //  return LecturePage14();
    default:
      return LecturePage('Лекция $lectureNumber');  // Возврат страницы с заголовком лекции, если программы нет
  }
}
