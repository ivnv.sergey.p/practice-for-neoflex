import 'package:flutter/material.dart';

// Эта страница отображает заголовок лекции, если нет программы
class LecturePage extends StatelessWidget {
  final String lectureName;

  LecturePage(this.lectureName);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(lectureName),
      ),
      body: Center(
        child: Text('Это страница $lectureName'),
      ),
    );
  }
}
