import 'package:flutter/material.dart';

class LecturePage5 extends StatefulWidget {
  @override
  _LecturePage5State createState() => _LecturePage5State();
}

class _LecturePage5State extends State<LecturePage5> {
  final List<String> _items = []; // Коллекция для хранения элементов
  final TextEditingController _itemController = TextEditingController();

  void _addItem() {
    final String? item = _itemController.text.isNotEmpty ? _itemController.text : null;

    if (item != null) {
      setState(() {
        _items.add(item);
      });
      _itemController.clear(); // Очистка текстового поля после добавления
    }
  }

  void _removeItem(int index) {
    setState(() {
      _items.removeAt(index); // Удаление элемента по индексу
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Лекция 5'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back), // Иконка стрелки назад
          onPressed: () {
            Navigator.pop(context); // Возврат на экран списка лекций
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            TextField(
              controller: _itemController,
              decoration: InputDecoration(labelText: 'Введите имя объекта'),
            ),
            SizedBox(height: 10),
            ElevatedButton(
              onPressed: _addItem,
              child: Text('Добавить'),
            ),
            SizedBox(height: 20),
            Expanded(
              child: ListView.builder(
                itemCount: _items.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: Text(_items[index]),
                    trailing: IconButton(
                      icon: Icon(Icons.delete),
                      onPressed: () => _removeItem(index), // Удаление элемента по нажатию
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

