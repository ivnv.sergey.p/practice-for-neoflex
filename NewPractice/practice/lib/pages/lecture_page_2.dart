import 'package:flutter/material.dart';

class LecturePage2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Лекция 2'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back), // Иконка стрелки назад
          onPressed: () {
            Navigator.pop(context); // Возврат на экран списка лекций
          },
        ),
      ),
      body: MainScreen(),
    );
  }
}

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ElevatedButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => SecondScreen()));
            },
            child: Text('Открыть второй экран'),
          ),
          ElevatedButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => ThirdScreen()));
            },
            child: Text('Открыть третий экран'),
          ),
        ],
      ),
    );
  }
}

class SecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Второй экран'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back), // Иконка стрелки назад
          onPressed: () {
            Navigator.pop(context); // Возврат на главный экран лекции
          },
        ),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Вернуться на главный экран'),
        ),
      ),
    );
  }
}

class ThirdScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Третий экран'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back), // Иконка стрелки назад
          onPressed: () {
            Navigator.pop(context); // Возврат на главный экран лекции
          },
        ),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Вернуться на главный экран'),
        ),
      ),
    );
  }
}
