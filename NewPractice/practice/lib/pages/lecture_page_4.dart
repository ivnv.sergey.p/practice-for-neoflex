import 'package:flutter/material.dart';

class LecturePage4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter Widgets Demo'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back), // Иконка стрелки назад
          onPressed: () {
            Navigator.pop(context); // Возврат на экран списка лекций
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),  // 2. Padding
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            // 1. Layout Widgets (Row & Column & Stack are used in this app)

            // Row with Center and Align inside
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  color: Colors.blue,
                  height: 50,
                  width: 50,
                  child: Center(   // 4. Center
                    child: Text('C'),
                  ),
                ),
                Align(            // 3. Align
                  alignment: Alignment.bottomRight,
                  child: Container(
                    color: Colors.red,
                    height: 50,
                    width: 50,
                    child: Text('A'),
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),  // Spacing between Row and Stack

            // Stack with Positioned Containers
            Expanded(            // 8. Expanded to take flexible height
              flex: 1,
              child: Stack(       // 9. Stack
                children: <Widget>[
                  Container(       // 5. Container
                    color: Colors.green,
                  ),
                  Positioned(
                    top: 10,
                    left: 10,
                    child: Container(
                      color: Colors.orange,
                      height: 50,
                      width: 50,
                      child: Center(child: Text('1')),
                    ),
                  ),
                  Positioned(
                    bottom: 10,
                    right: 10,
                    child: Container(
                      color: Colors.purple,
                      height: 50,
                      width: 50,
                      child: Center(child: Text('2')),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10),  // Spacing between Stack and ListView

            // Column and ListView.builder
            Expanded(
              flex: 2,
              child: Column(       // 7. Column with ListView inside
                children: <Widget>[
                  Text('Scrollable List:'),
                  Expanded(
                    child: ListView.builder(  // 10. ListView
                      itemCount: 5,
                      itemBuilder: (context, index) {
                        return Padding(       // 2. Padding inside ListView
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            height: 50,
                            color: Colors.grey[(index + 1) * 200],
                            child: Center(
                              child: Text('Item ${index + 1}'),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

