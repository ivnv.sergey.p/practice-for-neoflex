import 'package:flutter/material.dart';

class LecturePage6 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Лекция 6'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Это страница Лекции 6'),

            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => HomeScreen()), // Переход к главному экрану
                );
              },
              child: Text('Перейти на Главный Экран'),
            ),
          ],
        ),
      ),
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Главный Экран')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => SecondScreen())); // Переход на второй экран
              },
              child: Text('Перейти на Второй Экран'),
            ),
            ElevatedButton(
              onPressed: () async {
                final result = await Navigator.push(context, MaterialPageRoute(builder: (context) => ThirdScreen(data: 'пример данных'))); // Передача данных на третий экран
                if (result != null) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('Возвращено: $result')),
                  );
                }
              },
              child: Text('Перейти на Третий Экран'),
            ),
            ElevatedButton(
              onPressed: () async {
                final result = await Navigator.push(context, MaterialPageRoute(builder: (context) => FourthScreen())); // Переход на четвертый экран
                if (result != null) {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(content: Text('Возвращено из Четвертого Экрана: $result')),
                  );
                }
              },
              child: Text('Перейти на Четвертый Экран'),
            ),
          ],
        ),
      ),
    );
  }
}

class SecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Второй Экран')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Это Второй Экран'),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context, 'Привет с Второго Экрана'); // Возврат данных на предыдущий экран
              },
              child: Text('Назад на Главный Экран'),
            ),
          ],
        ),
      ),
    );
  }
}

class ThirdScreen extends StatelessWidget {
  final String data;

  const ThirdScreen({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Третий Экран')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Полученные Данные: $data'), // Отображение полученных данных
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context, 'Данные возвращены с Третьего Экрана'); // Возврат данных на предыдущий экран
              },
              child: Text('Назад на Главный Экран'),
            ),
          ],
        ),
      ),
    );
  }
}

class FourthScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Четвертый Экран')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Это Четвертый Экран'),
            ElevatedButton(
              onPressed: () {
                Navigator.pop(context, 'Данные возвращены с Четвертого Экрана'); // Возврат данных на предыдущий экран
              },
              child: Text('Назад на Главный Экран'),
            ),
          ],
        ),
      ),
    );
  }
}
