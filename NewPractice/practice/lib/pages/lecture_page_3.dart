import 'package:flutter/material.dart';
import 'dart:math';

class LecturePage3 extends StatefulWidget {
  @override
  _LecturePage3State createState() => _LecturePage3State();
}

class _LecturePage3State extends State<LecturePage3> {
  late List<ColorTile> tiles;

  @override
  void initState() {
    super.initState();
    tiles = [
      ColorTile(key: UniqueKey()),
      ColorTile(key: UniqueKey()),
      ColorTile(key: UniqueKey()),
      ColorTile(key: UniqueKey()),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Лекция 3'),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),  // Иконка назад
          onPressed: () {
            Navigator.pop(context);  // Возвращение к предыдущему экрану
          },
        ),
      ),
      body: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: tiles,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: swapTiles,
        child: Icon(Icons.switch_access_shortcut),
      ),
    );
  }

  void swapTiles() {
    setState(() {
      final tile = tiles.removeAt(0);
      tiles.add(tile);
    });
  }
}

class ColorTile extends StatefulWidget {
  const ColorTile({super.key});

  @override
  State<ColorTile> createState() => _ColorTileState();
}

class _ColorTileState extends State<ColorTile> {
  late Color color;

  @override
  void initState() {
    super.initState();
    color = Colors.primaries[Random().nextInt(Colors.primaries.length)];
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 100,
      height: 100,
      color: color,
    );
  }
}
